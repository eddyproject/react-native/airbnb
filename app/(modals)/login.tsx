import {StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {useWarmUpBrowser} from "@/hooks/useWarmUpBrowser";

import {defaultStyles} from '@/constants/Styles';
import Colors from "@/constants/Colors";
import {Ionicons} from "@expo/vector-icons";
import {useRouter} from "expo-router";
import {useOAuth} from "@clerk/clerk-expo";

enum Strategy {
    Google = 'oauth_google',
    Apple = 'oauth_apple',
    Facebook = 'oauth_facebook',
}

const Login = () => {
    useWarmUpBrowser();

    const router = useRouter();
    const { startOAuthFlow: googleAuth } = useOAuth({ strategy: 'oauth_google' });
    const { startOAuthFlow: appleAuth } = useOAuth({ strategy: 'oauth_apple' });
    const { startOAuthFlow: facebookAuth } = useOAuth({ strategy: 'oauth_facebook' });

    const onSelectAuth = async (strategy: Strategy) => {
        const selectedAuth = {
            [Strategy.Google]: googleAuth,
            [Strategy.Apple]: appleAuth,
            [Strategy.Facebook]: facebookAuth,
        }[strategy];

        try {
            const { createdSessionId, setActive } = await selectedAuth();

            if (createdSessionId) {
                await setActive!({session: createdSessionId});
                router.back();
            }
        } catch (err) {
            console.error('OAuth error', err);
        }
    };

    return (
        <View style={styles.container}>
            <TextInput autoCapitalize="none" placeholder="Email"
                       style={[defaultStyles.inputField, {marginBottom: 30}]}></TextInput>
            <TouchableOpacity style={defaultStyles.btn}>
                <Text style={defaultStyles.btnText}>Continue</Text>
            </TouchableOpacity>
            <View style={styles.separateView}>
                <View style={{
                    flex: 1,
                    borderBottomColor: '#000',
                    borderBottomWidth: StyleSheet.hairlineWidth
                }}>
                </View>
                <Text style={styles.separator}>or</Text>
                <View style={{
                    flex: 1,
                    borderBottomColor: '#000',
                    borderBottomWidth: StyleSheet.hairlineWidth
                }}>
                </View>
            </View>

            <View style={{gap:20}}>
                <TouchableOpacity style={styles.btnOutline}>
                    <Ionicons name='call-outline' style={defaultStyles.btnIcon} size={24}/>
                    <Text style={styles.btnOutlineText}>Continue with Phone</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnOutline} onPress={() => onSelectAuth(Strategy.Apple)}>
                    <Ionicons name="logo-apple" size={24} style={defaultStyles.btnIcon} />
                    <Text style={styles.btnOutlineText}>Continue with Apple</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnOutline} onPress={() => onSelectAuth(Strategy.Google)}>
                    <Ionicons name='logo-google' style={defaultStyles.btnIcon} size={24}/>
                    <Text style={styles.btnOutlineText}>Continue with Google</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnOutline} onPress={() => onSelectAuth(Strategy.Facebook)}>
                    <Ionicons name='logo-facebook' style={defaultStyles.btnIcon} size={24}/>
                    <Text style={styles.btnOutlineText}>Continue with Facebook</Text>
                </TouchableOpacity>
            </View>


        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        padding: 26,
    },
    separateView: {
        flexDirection: 'row',
        alignItems: "center",
        gap: 10,
        marginVertical: 30,
    },
    separator: {
        fontFamily: 'mon-sb',
        color: Colors.grey,
    },
    btnOutline: {
        backgroundColor: Colors.white,
        borderWidth: 1,
        borderColor: Colors.grey,
        height: 50,
        borderRadius: 8,
        justifyContent: 'center',
        flexDirection: 'row',
        paddingHorizontal: 10,
        alignItems: 'center',
    },
    btnOutlineText: {
        color: Colors.black,
        fontSize: 16,
        fontFamily: 'mon-b',
    },
});
export default Login;
