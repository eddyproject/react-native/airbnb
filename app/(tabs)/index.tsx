import {View} from "react-native";
import {Stack} from "expo-router";
import ExploreHeader from "@/components/ExploreHeader";
import {useMemo, useState} from "react";
import listingsData from "@/assets/data/airbnb-listings.json";
import listingsDataGeo from "@/assets/data/airbnb-listings.geo.json";
import ListingsMap from "@/components/ListingsMap";
import ListingsBottomSheet from "@/components/ListingsBottomSheet";
import {GestureHandlerRootView} from "react-native-gesture-handler";

const Page = () => {
    const [category, setCategory] = useState('Tiny homes');

    const items = useMemo(() => listingsData as any, []);
    const listingsGeo = useMemo(() => listingsDataGeo as any, []);
    const onDataChanged = (category: string) => {
        setCategory(category);
    };
    return (
        <GestureHandlerRootView style={{flex: 1}}>
            <View style={{flex: 1, marginTop: 80}}>
                <Stack.Screen options={{
                    header: () => <ExploreHeader onCategoryChange={onDataChanged}/>
                }}/>
                <ListingsMap listings={listingsGeo}/>
                <ListingsBottomSheet listings={items} category={category}/>
            </View>
        </GestureHandlerRootView>
    );
};

export default Page;
