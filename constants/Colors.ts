export default {
  primary:'#ff385c',
  grey:'#5e5d5d',
  dark:'#1a1a1a',
  white:'#fff',
  black:'#000',
};
