import {StyleSheet, Text, View} from "react-native";
import {Marker, PROVIDER_GOOGLE} from "react-native-maps";
import {defaultStyles} from "@/constants/Styles";
import {memo, useRef} from "react";
import {useRouter} from "expo-router";
import Colors from "@/constants/Colors";
import MapView from 'react-native-map-clustering'

interface Props {
    listings: any;
}

const INITIAL_REGION = {
    latitude: 52.52,
    longitude: 13.40,
    latitudeDelta: 9,
    longitudeDelta: 9,
};

const ListingsMap = memo(({listings}: Props) => {
    const router = useRouter();
    const mapRef = useRef<any>(null);

    // When the component mounts, locate the user
    const onMarkerSelected = (item: any) => {
        // @ts-ignore
        router.push(`/listing/${item.properties.id}`)
    };

    const renderCluster = (cluster: any) => {
        const { id, geometry, onPress, properties } = cluster;

        const points = properties.point_count;
        return (
            <Marker
                key={`cluster-${id}`}
                coordinate={{
                    longitude: geometry.coordinates[0],
                    latitude: geometry.coordinates[1],
                }}
                onPress={onPress}>
                <View style={styles.marker}>
                    <Text
                        style={{
                            color: '#000',
                            textAlign: 'center',
                            fontFamily: 'mon-sb',
                        }}>
                        {points}
                    </Text>
                </View>
            </Marker>
        );
    };
    return (
        <View style={defaultStyles.container}>
            <MapView
                animationEnabled={false}
                style={StyleSheet.absoluteFill}
                showsUserLocation
                showsMyLocationButton
                provider={PROVIDER_GOOGLE}
                initialRegion={INITIAL_REGION}
                clusterColor={Colors.white}
                clusterTextColor={Colors.black}
                clusterFontFamily='mon-sb'
                renderCluster={renderCluster}
            >
                {listings.features.map((item: any) => (
                    <Marker
                        coordinate={{
                            latitude: +item.properties.latitude,
                            longitude: +item.properties.longitude,
                        }}
                        key={item.properties.id}
                        onPress={() => onMarkerSelected(item)}>
                        <View style={styles.marker}>
                            <Text style={styles.markerText}>${item.properties.price}</Text>
                        </View>
                    </Marker>
                ))}
            </MapView>
        </View>
    );
});

const styles = StyleSheet.create({
    marker: {
        backgroundColor: Colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 6,
        paddingHorizontal: 12,
        borderRadius: 20,
        borderColor: Colors.black,
        borderWidth: 0.5,
        elevation: 5,
        shadowColor: Colors.black,
        shadowOpacity: 0.1,
        shadowRadius: 12,
        shadowOffset: {width: 1, height: 10},
    },
    markerText: {
        fontSize: 14,
        fontFamily: 'mon-sb',
    },
    locateBtn: {
        position: 'absolute',
        top: 70,
        right: 20,
        backgroundColor: '#fff',
        padding: 10,
        borderRadius: 10,
        elevation: 2,
        shadowColor: '#000',
        shadowOpacity: 0.1,
        shadowRadius: 6,
        shadowOffset: {
            width: 1,
            height: 10,
        },
    }
})

export default ListingsMap;