import {Platform, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import Colors from "@/constants/Colors";
import {Link} from "expo-router";
import {Ionicons, MaterialIcons} from "@expo/vector-icons";
import {useRef, useState} from "react";
import * as Haptics from "expo-haptics";

const categories = [
    {
        name: 'Tiny homes',
        icon: 'home',
    },
    {
        name: 'Cabins',
        icon: 'house-siding',
    },
    {
        name: 'Trending',
        icon: 'local-fire-department',
    },
    {
        name: 'Play',
        icon: 'videogame-asset',
    },
    {
        name: 'City',
        icon: 'apartment',
    },
    {
        name: 'Beachfront',
        icon: 'beach-access',
    },
    {
        name: 'Countryside',
        icon: 'nature-people',
    },
];

interface Props {
    onCategoryChange: (category: string) => void;
}

export const ExploreHeader = ({onCategoryChange}: Props) => {
    const scrollRef = useRef<ScrollView | null>(null);
    const itemsRef = useRef<Array<TouchableOpacity | null>>([]);
    const [activeIndex, setActiveIndex] = useState(0);
    const selectCategory = (index: number) => {
        const selected = itemsRef.current[index];
        setActiveIndex(index);
        selected?.measure((x) => {
            scrollRef.current?.scrollTo({x: x - 16, y: 0, animated: true})
        });
        Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
        onCategoryChange(categories[index].name);
    }
    return (
        <SafeAreaView style={[{flex: 1, backgroundColor: Colors.white}, styles.AndroidSafeArea]}>
            <View style={styles.container}>
                <View style={styles.actionRow}>
                    <Link href={'/(modals)/booking'} asChild>
                        <TouchableOpacity style={styles.searchBtn}>
                            <Ionicons name='search' size={24}/>
                            <View>
                                <Text style={{fontFamily: 'mon-sb'}}>Where to?</Text>
                                <Text style={{fontFamily: 'mon', color: Colors.grey}}>Anywhere . any week</Text>
                            </View>
                        </TouchableOpacity>
                    </Link>
                    <TouchableOpacity style={styles.filterBtn}>
                        <Ionicons name='options-outline' size={24}/>
                    </TouchableOpacity>
                </View>
                <ScrollView
                    ref={scrollRef}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{
                        alignItems: 'center',
                        gap: 25,
                        paddingHorizontal: 16,
                    }}>
                    {categories.map((category, index) => (
                        <TouchableOpacity
                            onPress={() => selectCategory(index)}
                            key={index}
                            ref={(el) => itemsRef.current[index] = el}
                            style={activeIndex === index ? styles.categoriesBtnActive : styles.categoriesBtn}>
                            <MaterialIcons name={category.icon as any} size={24}
                                           color={activeIndex === index ? Colors.black : Colors.grey}/>
                            <Text
                                style={activeIndex === index ? styles.categoryTextActive : styles.categorytext}>{category.name}</Text>
                        </TouchableOpacity>
                    ))}
                </ScrollView>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    AndroidSafeArea: {
        flex: 1,
        backgroundColor: Colors.white,
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight! + 40 : 0,
    },
    container: {
        backgroundColor: Colors.white,
        height: 130,
        elevation: 2,
        shadowColor: '#000',
        shadowOpacity: 0.1,
        shadowRadius: 6,
        shadowOffset: {
            width: 1,
            height: 10,
        },
    },
    actionRow: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 24,
        paddingBottom: 16,
        gap: 10,
    },
    filterBtn: {
        padding: 10,
        borderWidth: 1,
        borderColor: Colors.grey,
        borderRadius: 24,
    },
    searchBtn: {
        flexDirection: 'row',
        alignItems: 'center',
        gap: 10,
        borderColor: '#c2c2c2',
        borderWidth: StyleSheet.hairlineWidth,
        flex: 1,
        padding: 14,
        borderRadius: 24,
        backgroundColor: Colors.white,
        elevation: 2,
        shadowColor: Colors.black,
        shadowOpacity: 0.12,
        shadowRadius: 8,
        shadowOffset: {
            width: 1,
            height: 1,
        },
    },
    categorytext: {
        fontSize: 14,
        fontFamily: 'mon-sb',
        color: Colors.grey,
    },
    categoryTextActive: {
        fontSize: 14,
        fontFamily: 'mon-sb',
        color: '#000',
    },
    categoriesBtn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 8,
    },
    categoriesBtnActive: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingBottom: 8,
        borderBottomWidth: 2,
        borderBottomColor: Colors.black,
    }
})
export default ExploreHeader;
